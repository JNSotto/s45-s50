const coursesData = [
	{
		id:"wdc001",
		name:"PHP- Laravel",
		description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa quia accusamus nam quo similique assumenda ratione excepturi dolores ipsum aperiam cupiditate, ducimus cumque, consequuntur nobis aspernatur deserunt omnis adipisci vel!",
		price: 45000,
		onOffer: true
	},	
	{
		id:"wdc002",
		name:"Python - Django",
		description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa quia accusamus nam quo similique assumenda ratione excepturi dolores ipsum aperiam cupiditate, ducimus cumque, consequuntur nobis aspernatur deserunt omnis adipisci vel!",
		price: 55000,
		onOffer: true
	},	
	{
		id:"wdc003",
		name:"Java - Springboot",
		description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa quia accusamus nam quo similique assumenda ratione excepturi dolores ipsum aperiam cupiditate, ducimus cumque, consequuntur nobis aspernatur deserunt omnis adipisci vel!",
		price: 60000,
		onOffer: true
	}
]

export default coursesData;