
import {useState,useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Redirect} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'


export default function Login(){

	//Allow us to consume the User Context object and it's properties to use for user validation
	const {user, setUser} = useContext(UserContext)
	const [password,setPassword] = useState('')
	const [email,setEmail] = useState('')
	const [isActive, setIsActive] = useState(false)

	console.log(email)
	console.log(password)

	function Aunthenticate(e){
		
		//Prevent page redirection via form submission
		e.preventDefault();

		fetch('http://localhost:4000/users/login',{
			method:'POST',
			headers:{
				'Content-Type' : 'application/json'
			},
			body:JSON.stringify({
				email:email,
				password:password
			})
		})

		.then(res => res.json())
		.then(data =>{
			console.log(data)

			if(typeof data.access !== 'undefined'){
				localStorage.setItem('token',data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title:'Login Successful!',
					icon:'success',
					text:'Welcome to Zuitt'
				})
			} else {
				Swal.fire({
					title:'Authentication Failed',
					icon:'error',
					text:'Check your login details'
			})
			}
		})
		//Set the email of the user in the local storage
		//Syntax: localStarage.setItem('propertyName', value)
		//localStorage.setItem('email',email);

		/*setUser({
			email:localStorage.getItem('email')
		})*/
		

		setEmail('');
		setPassword('');

		//alert('You Are Now Logged In');

		const retrieveUserDetails = (token) => {
			fetch('http://localhost:4000/users/details',{
				method:'GET',
				headers:{
					Authorization:`Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data =>{
				console.log(data)

				setUser({
					id:data._id,
					isAdmin:data.isAdmin
				})
			})
		}

	}

	useEffect(() => {

    // Validation to enable submit button when all fields are populated and both passwords match
    if(email !== '' && password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }

	}, [email, password]);


	return(
		(user.id !==null) ?
			<Redirect to = "/courses"/>
			:
			<Form onSubmit = {(e) => Aunthenticate(e)}>
				<Form.Group className='my-1'>
					<Form.Label>Email Address:</Form.Label>
					<Form.Control
					 type='email'
					 placeholder='Please enter your email here'
					 value = {email}
					 onChange = {event => setEmail(event.target.value)}
					 required
					/>
				</Form.Group>

				<Form.Group controlId ="password" className='my-1'>
					<Form.Label>Password:</Form.Label>
					<Form.Control
						type = 'password'
						placeholder = 'Please Input your Password here'
						value = {password}
						onChange = {event => setPassword(event.target.value)}
						required
					 />
				</Form.Group>

				{ isActive ? 
					<Button variant = 'primary' type = 'submit' id = 'submitBtn' className='my-1'>
							Login
					</Button>

					:
					<Button variant = 'danger' type = 'submit' id = 'submitBtn' className='my-1' disabled>
						Login
					</Button>
				}
			</Form>
		)
}
