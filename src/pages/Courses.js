import {Fragment, useEffect, useState} from 'react'
// import coursesData from '../data/coursesData'
import CourseCard from '../components/CourseCard'

export default function Courses(){
	
	// console.log(coursesData)
	// console.log(coursesData[0])

//.map method only use in ARRAY
const [courses, setCourses] = useState([])

useEffect(()=> {
/*	fetch('http://localhost:4000/courses/all',{
		header: {
			Authorization: `Bearer ${localStorage.getItem("token")}`
		}
	})	*/
	fetch('http://localhost:4000/courses/all')
	.then(res => res.json())
	.then(data => {
		console.log(data)
	
	setCourses(data.map(course => {
	return(
		<CourseCard key = {course.id} courseProp = {course}/>
		)
		}))
	})
},[])

/*const courses = coursesData.map(course => {
	return(
		<CourseCard key = {course.id} courseProp = {course}/>
		)
})*/

	return(
		<Fragment>
			<h1>Courses</h1>
			{courses}
			{/*<CourseCard courseProp = {coursesData[0]}/>*/}
		</Fragment>
		)
}