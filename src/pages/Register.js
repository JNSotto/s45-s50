import {useState,useEffect,useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Redirect,useHistory} from 'react-router-dom' //6.2.0 Navigate 
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function Register(){

	const {user, setUser} = useContext(UserContext)

	const history = useHistory()

	//State hooks to store the values of the input fields
	const [firstName,setFirstName] = useState('')
	const [lastName,setLastName] = useState('')
	const [email,setEmail] = useState('')
	const [mobileNo,setMobileNo] = useState('')
	const [password1,setPassword1] = useState('')
	const [password2,setPassword2] = useState('')

	//State to determine whether register button is enabled or not
	const [isActive, setIsActive] = useState(false)

	//Check if values are successfully binded
	console.log(email)
	console.log(password1)
	console.log(password2)

	//this is for clearing the input 
	function registerUser(e){
		
		//Prevents page redirection via form submission
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail',{
				method:'POST',
				headers:{
					'Content-Type' :'application/json'
				},
				body:JSON.stringify({
					email:email
				})
			})
			.then(res => res.json())
			.then(retrieve =>{
				if( retrieve !== true){
				fetch('http://localhost:4000/users/register',{
					method:'POST',
					headers:{
						'Content-Type' : 'application/json'
					},
					body:JSON.stringify({
						firstName:firstName,
						lastName:lastName,
						email:email,
						mobileNo:mobileNo,
						password:password1
					})
				})

				.then(res => res.json())
				.then(register =>{
					console.log(register)
				Swal.fire({
					title:'Registration Successful!',
					icon:'success',
					text:'Welcome to Zuitt'
				})
				history.push("/login")
				})
			} else {
				Swal.fire({
					title:'Email Already Exist!',
					icon:'error',
					text:'Please try to another email'
			})
			}
		})

			
		//Clear the input fields
		setFirstName('');
		setLastName('');
		setEmail('');
		setMobileNo('');
		setPassword1('');
		setPassword2('');

		//alert('Thank you for Registering!');



	}

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)) {

			setIsActive(true)

		} else {

			setIsActive(false)
		}
	}, [firstName,lastName,email,mobileNo,password1,password2])

	return((user.id !==null) ?
			<Redirect to = "/courses"/>
			:
			<Form onSubmit = {(e) => registerUser(e)}>
				<Form.Group className='my-1'>
					<Form.Label>First Name:</Form.Label>
					<Form.Control
					 type='firstName'
					 placeholder='Please enter your First Name here'
					 value = {firstName}
					 onChange = {event => setFirstName(event.target.value)}
					 required
					/>
				</Form.Group>				

			<Form.Group className='my-1'>
					<Form.Label>Last Name:</Form.Label>
					<Form.Control
					 type='lastName'
					 placeholder='Please enter your Last Name here'
					 value = {lastName}
					 onChange = {event => setLastName(event.target.value)}
					 required
					/>
				</Form.Group>				

			<Form.Group className='my-1'>
					<Form.Label>Email Address:</Form.Label>
					<Form.Control
					 type='email'
					 placeholder='Please enter your email here'
					 value = {email}
					 onChange = {event => setEmail(event.target.value)}
					 required
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>				

			<Form.Group className='my-1'>
					<Form.Label>Mobile Number:</Form.Label>
					<Form.Control
					 type='mobileNo'
					 placeholder='Please enter your Mobile Number here'
					 value = {mobileNo}
					 onChange = {event => setMobileNo(event.target.value)}
					 required
					/>
				</Form.Group>

				<Form.Group controlId ="password1" className='my-1'>
					<Form.Label>Password:</Form.Label>
					<Form.Control
						type = 'password'
						placeholder = 'Please Input your Password here'
						value = {password1}
						onChange = {event => setPassword1(event.target.value)}
						required
					 />
				</Form.Group>

				<Form.Group controlId ="password2" className='my-1'>
					<Form.Label>Verify Password:</Form.Label>
					<Form.Control
						type= 'password'
						placeholder = 'Please verify your password'
						value = {password2}
						onChange = {event => setPassword2(event.target.value)}
						required
					/>
				</Form.Group>

				{ isActive ? 
					<Button variant = 'primary' type = 'submit' id = 'submitBtn' className='my-1'>
							Register
					</Button>

					:
					<Button variant = 'danger' type = 'submit' id = 'submitBtn' className='my-1' disabled>
						Register
					</Button>
				}
			</Form>

		
		)
}


/*
Ternary Operator with the Botton Tag

'?' - if --> if <Button is isActive, the color will be primary and it is enabled> 
':' - else --> else <Button will be color danger and will be disabled

'No braces needed'

useEffect - react hook
Syntax: useEffect(()=>{
	<condition>
},[parameter])

-- once a change happens in the parameter, the useEffect will run.
*/
