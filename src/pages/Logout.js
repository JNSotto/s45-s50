import {useEffect, useContext} from 'react'
import {Redirect} from 'react-router-dom'
import UserContext from '../UserContext'


export default function Logout(){

	//Consume the UserContext object and destructure it to access to user state and unsetUser function from the context provider
	const {unsetUser, setUser} = useContext(UserContext)

	//localStorage.clear();
	
	//Clear the localStorage of the user's information
	unsetUser()

	//Placing the "setUser" setter function inside the useEffect is necessary because of updates within ReactJS that a state of another component cannot be updated while trying to render a different component
	//By adding the useEffect, this will allow the logout page to render first before triggering the useEffect which changes the state of our user.
	useEffect( () =>{
		//Set the user state back to its original value
		setUser({id:null})
	}) //No need for parameter because it is only one time use. 

	//Redirect allows us to navigate to a new location. 
	return(
			<Redirect to='/login'/>
		)

}

