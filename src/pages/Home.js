/*import {Fragment} from 'react'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'

export default function Home(){
	return(
			<Fragment>
				<Banner/>
				<Highlights/>
			</Fragment>
		)
}*/

//Solution
import {Fragment} from 'react';
import Banner from '../components/Banner';
// import CourseCard from '../components/CourseCard';
import Highlights from '../components/Highlights';


export default function Home (){

	const data={
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere",
		destination: "/",
		label: "Enroll now!"
	}


	return(
		<Fragment>
			<Banner data={data} />
			<Highlights/>
			{/*<CourseCard/>*/}
		</Fragment>
	)
}
