//'import' is like 'require' in express.js
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);



//For Example
/*const name = 'Jan Smith'

const user = {
  firstName: 'Solar',
  lastName: 'Stone'
}

function formatName(user){
  return user.firstName + ' ' + user.lastName
}

const element = <h1> Hello, {formatName(user)}</h1>

ReactDOM.render(
    element,
    document.getElementById('root')
  )
*/

