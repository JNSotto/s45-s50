//Module is considered to be an Object, that is why you need to object destruction 
//import {Fragment} from 'react'
import {useState, useEffect}  from 'react'
import {Container} from 'react-bootstrap'
import AppNavbar from './components/AppNavbar'
import {BrowserRouter as Router} from 'react-router-dom'
import{Route, Switch} from 'react-router-dom'
//import Banner from './components/Banner'
//import Highlights from './components/Highlights'
import Courses from './pages/Courses'
import CourseView from './pages/CourseView'
import ErrorPage from './pages/ErrorPage'
import Home from './pages/Home'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Register from './pages/Register'
import './App.css';
import {UserProvider} from './UserContext'


/*function App() {
  return (
  //if you put more than one component, put a parent enclosing tag
    <Fragment>
      <AppNavbar/>
      <Container>
       //<Banner/>
      //<Highlights/>
        <Register/>
        <Login/>
        <Home/>
        <Courses/>
      </Container>
    </Fragment>
  );
}*/

function App(){

  //State hook for the user state that's define here for a global scope 
  //Initialized as an object with properties from the localStorage 
  //This will be used to storage the information and will be used for validating if a user logged in on the app or not. 
  const[user, setUser] = useState({
    //email:localStorage.getItem('email')
    id:null,
    isAdmin:null
  })


  //Function for clearing localStorage
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(()=>{
    console.log(user)
    console.log(localStorage)
  },[user])

  //The UserProvider component is what allows other components to consume or use our context. Any component which is not wrapped by UserProvider will not have access to the vlues provided for our context. 
  
  // You can pass data or information to our context by providing a "value" attribute in our UserProvider. Data passed here can be accessed by other components by unwrapping our context using the useContext hook. 

  return(
    <UserProvider value = {{user,setUser,unsetUser}}>
        <Router>
          <AppNavbar/>
        <Container>
          <Switch>
            <Route exact path="/" component = {Home}/>
            <Route exact path="/courses" component = {Courses}/>
            <Route exact path="/courses/:courseId" component = {CourseView}/>
            <Route exact path="/login" component = {Login}/>
            <Route exact path="/Register" component = {Register}/>
            <Route exact path="/logout" component = {Logout}/>
            <Route path="*" component = {ErrorPage}/>
          </Switch>
        </Container>
        </Router>
    </UserProvider>
    )
}

export default App;

/*
  React JS
  - is a single page application(SPA). However, we can simulate the changing of pages. 

  - We don't actually create new pages, what we just do is switch pages according to their assigned routes.

  -React JS and react-router-dom package just mimics or mirrors how HTML access its URL.

  React-Router-Dom 
  - 3 main components to simulate the changing of page 

    1. Router - wrapping the router component around other components  will allow us to use routing within our page. 

    2. Switch - Allow us to switch/change our page components

    3. Route - Assigns a path which will trigger the change/switch of components render. 

 */
