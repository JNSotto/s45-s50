/*import {Row, Col, Button} from 'react-bootstrap'

export default function Banner() {
	return(
		<Row>
			<Col className = "p-5">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere,</p>
				<Button variant = "primary">Enroll Now</Button>
			</Col>
		</Row>
		)
    }
*/
//Solution
import {Row, Col, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}){

	//console.log(data);
	const {title, content, destination, label} = data;


	return(
	<Row>
		<Col className = "p-5">
			<h1>{title}</h1>
			<p>{content}</p>
			<Link to={destination}>
				<Button variant="primary" to={destination}>{label}</Button>
			</Link>
		</Col>
	</Row>

	)
}

