import {useState} from 'react'
import {Card} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function CourseCard ({courseProp}){
	console.log(courseProp)

//Object destructuring, Syntax: const {properties} = propname
const {name,description,price, _id} = courseProp



	return(
		 		<Card>
		 			<Card.Body >
		 				<Card.Title>
		 					<h3>{name}</h3>
		 				</Card.Title>
		 				<Card.Subtitle>
		 					Description:
		 				</Card.Subtitle>
		 				<Card.Text>
		 					{description}
		 				</Card.Text>
		 				<Card.Subtitle>
		 					Price:
		 				</Card.Subtitle>
		 				<Card.Text>
		 					{price}
		 				</Card.Text>		 	
		 				<Link className = "btn btn-primary" to = {`/courses/${_id}`}>Details</Link>
		 			</Card.Body>
       			 </Card>


	)
}