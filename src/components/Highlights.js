import {Row, Col, Card} from 'react-bootstrap'
export default function Highlights(){
	return(
		 <Row className="mt-3 mb-3">
		 	<Col xs = {12} md = {4}>
		 		<Card className = "cardHighlight p-3">
		 			<Card.Body>
		 				<Card.Title>
		 					<h2>Learn From Home</h2>
		 				</Card.Title>
		 			<Card.Text>
		 				Lorem ipsum dolor sit amet, consectetur adipisicing, elit. Exercitationem, commodi labore incidunt saepe iusto facilis laboriosam perferendis omnis tempore sequi molestias, error voluptate fugiat deleniti sapiente, atque! Animi, nostrum, provident.
		 			</Card.Text>
		 			</Card.Body>
		 		</Card>
		 	</Col>
		 	<Col xs = {12} md = {4}>
		 		<Card className = "cardHighlight p-3">
		 			<Card.Body>
		 				<Card.Title>
		 					<h2>Study Pay Later</h2>
		 				</Card.Title>
		 				<Card.Text>
		 				Lorem ipsum dolor sit amet, consectetur adipisicing, elit. Exercitationem, commodi labore incidunt saepe iusto facilis laboriosam perferendis omnis tempore sequi molestias, error voluptate fugiat deleniti sapiente, atque! Animi, nostrum, provident.
		 				</Card.Text>
		 			</Card.Body>
		 		</Card>
		 	</Col>
		 	<Col xs = {12} md = {4}>
		 		<Card className = "cardHighlight p-3">
		 			<Card.Body>
		 				<Card.Title>
		 					<h2>Be Part of Our Community</h2>
		 				</Card.Title>
		 				<Card.Text>
		 					Lorem ipsum dolor sit amet, consectetur adipisicing, elit. Exercitationem, commodi labore incidunt saepe iusto facilis laboriosam perferendis omnis tempore sequi molestias, error voluptate fugiat deleniti sapiente, atque! Animi, nostrum, provident.
		 				</Card.Text>
		 			</Card.Body>
		 		</Card>
		 	</Col>
		 </Row>
		)
}
